
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/movies'

describe Movie do
  let(:title)  { 'Some Title' }
  let(:rating) { 10 }
  subject { Movie.new title, rating }

  describe 'an instance with title and rating' do
    it 'has a title attribute' do
      subject.must_respond_to 'title'
    end

    it 'has a rating attribute' do
      subject.must_respond_to 'rating'
    end

    it 'has a title correctly set' do
      subject.title.must_equal title
    end

    it 'has a rating correctly set' do
      subject.rating.must_equal rating
    end

    it 'returns a custom version string version with #to_s' do
      expected = "<#{subject.class}:title: #{title}, rating: #{rating}>"
      subject.to_s.must_equal expected
    end
  end

  describe 'an instance with only title' do
    let(:movie) { Movie.new 'Some Other Movie' }

    it 'has a default rating of 1' do
      movie.rating.must_equal 1
    end
  end

  describe 'instances are comparable'  do
    let(:instances) do
      [Movie.new('A',5), Movie.new('B',9), Movie.new('A',3)].sort
    end

    specify 'instances are sorted by rating' do
      instances[0].rating.must_be :<=, instances[1].rating
      instances[2].rating.must_be :>=, instances[1].rating
    end
  end
end
