
# Now that we have many of the basic classes of Ruby in our toolbox, try writing
# a program that uses modules and classes to create a movie class. Each movie
# should have a title and rating. Finally, implement comparison of movies using
# the Comparable module.

class Movie
  attr_reader :rating, :title

  include Comparable

  def initialize(title, rating = 1)
    @title   = title
    @rating = rating
  end

  def <=>(other_movie)
    self.rating <=> other_movie.rating
  end

  def to_s
    "<#{self.class}:title: #{@title}, rating: #{@rating}>"
  end
end

def run!
  movie1 = Movie.new 'Amelie', 10
  movie2 = Movie.new 'The Expendables', 3

  puts movie1
  puts movie2
  puts movie1 > movie2
  puts movie2 > movie1
  puts movie2 < movie1
end

run! if __FILE__ == $0
