
# Example on the use of symbols, the Enumerable and Comparable modules.

class BankAccount
  include Enumerable
  include Comparable

  def initialize(name)
    @name         = name
    @transactions = []
  end

  def <=>(other_account)
    self.balance <=> other_account.balance
  end

  def each
    @transactions.each { |transaction| yield transaction }
  end

  def deposit(amount, options = {})
    options[:memo] ||= 'Regular deposit'
    @transactions.push amount: amount, memo: options[:memo]
  end

  def withdraw(amount, options = {})
    options[:memo] ||= 'Regular withdraw'
    @transactions.push amount: -amount, memo: options[:memo]
  end

  def balance
    @transactions.inject(0) { |sum, transaction| sum += transaction[:amount] }
  end

  def to_s
    "<#{self.class}:name: #{@name}, balance: #{balance}>"
  end
end

account1 = BankAccount.new 'David'
account1.deposit  100
account1.withdraw  50
account1.deposit  500, memo: 'This was a gift'
account1.withdraw  21

account1.each { |transaction| puts transaction }

account2 = BankAccount.new 'Jubus'
account2.deposit 1000

account3 = BankAccount.new 'Mike The Frog'
account3.deposit 10_000

puts [account3, account1, account2].sort
puts [account3, account1, account2].max
puts [account3, account1, account2].min

puts account3 > account1
puts account2.between?(account1, account3)
