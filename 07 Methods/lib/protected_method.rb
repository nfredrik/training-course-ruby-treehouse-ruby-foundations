
# Now that we know the differences between the different kinds of methods, try
# writing a program with a protected method. Have that method raise a specific
# custom error if it's called inappropriately.

class Cell
  def message_a_peer(cell)
    cell.recieve_message
  end

  protected
  def recieve_message
    'Message received!'
  end

  private
  def method_missing(method_name, *args, &block)
    super
  rescue NoMethodError => error
    if error.message =~ /recieve_message/
      raise 'FORBIDEN! Unauthorized communication attempt'
    else
      raise error
    end
  end
end
