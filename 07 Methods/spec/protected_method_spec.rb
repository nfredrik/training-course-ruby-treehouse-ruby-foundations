
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/protected_method'

describe Cell do
  subject { Cell.new }

  it 'has a message_a_peer method' do
    subject.must_respond_to 'message_a_peer'
  end

  it 'raises error if receive_message is called from outside' do
    lambda { subject.receive_message }.must_raise NoMethodError
  end

  describe '#message_a_peer' do
    before { @other_cell = Cell.new }

    it 'confirms other cells has received a message' do
      confirmation = 'Message received!'
      subject.message_a_peer(@other_cell).must_equal confirmation
    end
  end
end
