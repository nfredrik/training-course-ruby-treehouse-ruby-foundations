
# More complex banking example.

class BankAccount
  class << self
    def create_for(first_name, last_name)
      @accounts ||=  []
      @accounts.push BankAccount.new(first_name, last_name)
    end

    def find_for(first_name, last_name)
      @accounts.find {|account| account.full_name == "#{first_name} #{last_name}"}
    end
  end

  attr_reader   :balance
  attr_accessor :transactions

  def initialie(first_name, last_name)
    @first_name = first_name
    @last_name  = last_name
    @balance    = 0
  end

  def full_name
    "#{@first_name} #{@last_name}"
  end

  def deposit(amount)
    @balance += amount
  end

  def withdraw(amount)
    @balance -= amount
  end

  def merge_account(account)
    self.deposit account.balance
    account.protected_reset!
  end

  def cancel_account
    private_reset!
  end

  protected
  def protected_reset!
    @balance = 0
  end

  private
  def private_reset!
    @balance = 0
  end
end
