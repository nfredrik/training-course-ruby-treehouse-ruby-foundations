
# Write a Ruby program that asks you for a string. Have the program remove any
# punctuation from the string and then print it out in reverse.

def run!
  print 'What do you have to say about anything? '
  answer = gets.chomp
  puts  'This is what you said, reversed and without punctuation:'
  puts akward_text(answer)
end

def akward_text(str)
  str.gsub(/[\.,:;¡!¿\?'"]/, '').reverse
end

run! if __FILE__ == $0
