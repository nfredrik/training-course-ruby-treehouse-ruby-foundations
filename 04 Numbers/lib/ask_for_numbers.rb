
# Try writing a Ruby program that asks a user to input numbers and then
# multiplies them together. Have the program print out the result to two decimal
# places regardless of whether or not the user entered an integer or floating
# point number.

def run!
  print 'How about if you give me a number? '
  number_one = gets.chomp.to_f
  print 'Let\'s multiply it with other one you pick: '
  number_two = gets.chomp.to_f
  puts  "The result is #{format_as_money(number_one * number_two)}"
end

def format_as_money(number)
  "#{sprintf '%.2f', number}"
end

run! if __FILE__ == $0
