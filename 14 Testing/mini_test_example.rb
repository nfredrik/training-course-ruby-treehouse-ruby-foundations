
# Example on the use of MiniTest.

require 'minitest/autorun'

class MyTest < MiniTest::Unit::TestCase
  def test_that_adition_works
    assert_equal 4, 2+2
  end

  def test_that_my_array_has_a_certain_item
    my_array = %w(cat dog frog)
    assert my_array.include? 'frog'
  end

  def test_assert_delta
    assert_in_delta 3.14159, 3, 0.2
  end

  def test_assert_match
    assert_match(/world/, 'Hello world')
  end

  def test_nil
    assert_nil nil
  end

  def test_output
    assert_output 'hello world', nil do
      print 'hello world'
    end
  end

  def test_raises
    assert_raises NameError do
      this_variable_does_not_exist
    end
  end

  def test_respond
    assert_respond_to Array.new, 'include?'
  end
end
