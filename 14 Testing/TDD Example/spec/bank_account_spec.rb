
require 'minitest/autorun'
require 'minitest/spec'
require_relative '../lib/bank_account'

describe BankAccount do
  subject { BankAccount.new 'David'}

  it 'should be a BankAccount instance' do
    subject.must_be_instance_of BankAccount
  end

  it 'should have a name' do
    subject.must_respond_to 'name'
  end

  it 'should initialize with name' do
    subject.name.must_equal 'David'
  end

  it 'should have transactions' do
    subject.must_respond_to 'transactions'
  end

  it 'should have and empty array of transactions' do
    subject.transactions.must_be_instance_of Array
    subject.transactions.must_equal []
  end

  describe '#deposit' do
    before { @account = BankAccount.new 'Jubus' }

    it 'responds to deposit' do
      @account.must_respond_to 'deposit'
    end

    it 'returns the amount of the deposit' do
      amount = 500
      @account.deposit(amount).must_equal amount
    end

    it 'adds the amount to the transactions' do
      amount = 111
      @account.deposit amount
      @account.transactions.must_include amount
    end
  end

  describe '#withdraw' do
    before { @account = BankAccount.new 'Ian' }

    it 'responds to withdraw' do
      @account.must_respond_to 'withdraw'
    end

    it 'returns the amount of the withdraw' do
      amount = 500
      @account.withdraw(amount).must_equal -amount
    end

    it 'adds the amount to the transactions' do
      amount = 111
      @account.withdraw amount
      @account.transactions.must_include -amount
    end
  end
end
