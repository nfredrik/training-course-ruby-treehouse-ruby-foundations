
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/redo_three_times'

describe '#stutter' do
  it 'keeps or adds to the length of the string' do
    phrase = 'Unit testing methods with indeterminate output.'
    stutter(phrase).length.must_be :>=, phrase.length
  end
end

describe '#stutter_word' do
  it 'adds the required repetitions to the given word' do
    word     = 'Hello'
    expected = 'H-H-Hello'
    stutter_word(word, 3).must_equal expected
  end
end
