
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/three_times'

describe '#three_times' do
  it 'outputs my name three times' do
    lambda { three_times }.must_output "David\nDavid\nDavid\n"
  end
end
