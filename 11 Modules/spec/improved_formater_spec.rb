
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/improved_formater'

class Dummy
  attr_accessor :name, :one, :two, :three
  extend  FormatAttributes
  include Formatter
  formats name: :h1, one: :h2, two: :div, three: :p
end

describe 'Formatter/FormatAttributes' do
  let(:attributes) { { name: :h1, one: :h2, two: :div, three: :p } }
  subject { Dummy.new }

  describe '.format_attributes' do
    specify 'host class have a .format_attributes method' do
      Dummy.must_respond_to 'format_attributes'
    end

    specify 'the class includes each of the given attributes' do
      attributes.each_key { |k| Dummy.format_attributes.must_include k }
    end
  end

  describe '#display' do
    specify 'host class instances have a #display method' do
      subject.must_respond_to 'display'
    end

    specify 'displays each attribute in the corresponding element' do
      expected = "<h1 id='name'/>\n<h2 id='one'/>\n<div id='two'/>\n" \
                 "<p id='three'/>\n"
      lambda { subject.display }.must_output expected
    end
  end
end
