
# An example of the use of blocks.

class Speech
  attr_reader :title

  def initialize
    print 'What is the speech name? '
    @title = gets.chomp
    @lines = []

    puts 'Line added.' while add_line
  end

  def add_line
    puts 'Add a line (blank line to exit):'
    line = gets.chomp

    (line.length > 0) ? @lines.push(line) : nil
  end

  def each(&block)
    @lines.each { |line| yield line }
  end
end

speech = Speech.new
speech.each { |line| puts "[#{speech.title}] #{line}" }
