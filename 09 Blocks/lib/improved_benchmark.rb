
# In this lesson, we've written a simple benchmarking class. Try expanding upon
# that by asking the user how many iterations they would like to run and then
# sorting the output from the quickest to slowest finding.

class ImprovedBenchmarker
  class << self
    def go(how_many = 1, &block)
      {}.tap do |iterations|
        how_many.times do |n|
          start  = Time.now
          block.call
          finish = Time.now
          iterations[n] = {
            start: start, finish: finish, elapsed: finish - start
          }
        end
      end
    end

    def print(results)
      puts "---------------- \t\tBenchmarking started \t\t-----------------"
      benchmarking_start = Time.now
      puts "Start Time:  #{benchmarking_start}\n"
      benchmarking_finish = Time.now
      puts "Finish Time: #{benchmarking_finish}\n"

      puts "Result:\t\t#{benchmarking_finish - benchmarking_start} seconds."
      puts "--------------- \t\tBenchmarking details \t\t------------------"
      puts "I Start\t\t\t\tFinish\t\t\t\tElapsed"
      results.sort_by { |_, v| v[:elapsed] }.each do |k, v|
        puts "#{k} #{v[:start]}\t#{v[:finish]}\t#{v[:elapsed]}"
      end
      puts "--------------- \t\tBenchmarking finished \t\t-----------------"
    end
  end
end

def run!
 ImprovedBenchmarker.print(ImprovedBenchmarker.go(5) { sleep rand(0.1..1.0) })
end

run! if __FILE__ == $0
