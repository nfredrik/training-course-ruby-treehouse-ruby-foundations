
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/improved_benchmark'

describe ImprovedBenchmarker do

  it 'has a .go method' do
    ImprovedBenchmarker.must_respond_to 'go'
  end

  it 'has a .print method' do
    ImprovedBenchmarker.must_respond_to 'print'
  end

  describe '.go' do
    let(:times) { 3 }
    subject { ImprovedBenchmarker.go(times) { sleep rand(0.1..1.0) } }

    specify 'the number of results is the same as the requested iterations' do
      subject.size.must_equal times
    end

    specify 'a result iteration has a :start field' do
      subject[0][:start].wont_be_nil
    end

    specify 'a result iteration has a :finish field' do
      subject[0][:finish].wont_be_nil
    end

    specify 'a result iteration has a :elapsed field' do
      subject[0][:elapsed].wont_be_nil
    end
  end
end
