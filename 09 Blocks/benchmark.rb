
# An example of the use of blocks.

class SimpleBenchmarker
  class << self
    def go(how_many = 1, &block)
      puts '--------------- Benchmarking started ----------------'
      start_time  = Time.now
      puts "Start Time: #{start_time}\n\n"
      how_many.times do
        print '.'
        block.call
      end
      finish_time = Time.now
      puts "Finish Time: #{finish_time}\n"
      puts '-------------- Benchmarking finished ----------------'
      puts "\n\n"
      puts "Result:\t\t#{finish_time - start_time} seconds."
    end
  end
end

SimpleBenchmarker.go(5) { sleep rand(0.1..1.0) }
