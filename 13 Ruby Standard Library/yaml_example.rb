
# Example on the use of YAML.

require 'yaml'

class Location
  def initialize(name, address, denizents = [])
    @name      = name
    @address   = address
    @denizents = denizents
  end
end

irapuato = Location.new 'Omar\'s Den', 'Santa Fe', ['Omar', 'Jubus', 'Rojo']

puts irapuato.inspect
puts irapuato.to_yaml

File.open('config.yml', 'w') { |f| f.puts irapuato.to_yaml }

location = YAML.load(File.read './config.yml')
puts location.inspect

location_copy = YAML::load(location.to_yaml)
puts location_copy.inspect
