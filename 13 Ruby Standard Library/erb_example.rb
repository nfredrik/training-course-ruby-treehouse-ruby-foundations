
# Example on the use of embedded Ruby.

require 'erb'

treehouse = { name: 'Treehouse',  location: 'Treehouse Island' }

template = <<-TEMPLATE
From the desk of <%= treehouse[:name] %>
-------------------------------------------
Welcome to <%= treehouse[:location]%>.

We hope you enjoy your stay.
-------------------------------------------
TEMPLATE

erb = ERB.new(template)
puts erb.result
