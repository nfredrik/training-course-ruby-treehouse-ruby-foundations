
# Example on the use of Benchmark.

require 'benchmark'

REPETITIONS = 1_000_000

Benchmark.bm(7) do |x|
  x.report 'String' do
    REPETITIONS.times { options = { 'hello' => 'world' } }
  end

  x.report 'Symbol' do
    REPETITIONS.times { options = { hello: :world } }
  end
end
