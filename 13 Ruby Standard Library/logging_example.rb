
# Example on the use of Logger.

require 'logger'

class BackAccount
  attr_reader :file_logger, :stdout_logger

  def initialize(name)
    @name         = name
    @transactions = []
    @stdout_logger = Logger.new STDOUT             # Log to standard output
    @file_logger   = Logger.new './bank_account.log' # Log to file
  end

  def deposit(amount)
    log "Depositing #{amount}"
    @transactions.push amount
  end

  def withdraw(amount)
    log "Withdrawing #{amount}"
    @transactions.push -amont
  end

  def log(message, level = Logger::INFO)
    file_logger.add(level, message, "#{self.class} (#{@name})")
    stdout_logger.add(level, message, "#{self.class} (#{@name})")
  end
end

account = BackAccount.new 'David'
account.deposit 100
account.deposit  50
