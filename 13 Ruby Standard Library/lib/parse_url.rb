
# Writing a program that reads from a public web page and looks for the title of
# the page. The program should then output the contents of the page title to
# STDOUT using an ERB template.

require 'logger'
require 'ostruct'
require 'optparse'
require 'open-uri'

class Options
  class << self
    def parse(args)
      options_set = OpenStruct.new

      options_available = OptionParser.new do |opts|
        opts.banner = 'Usage: url'

        opts.on('-u', '--url URL', 'Fetches the given url') do |url|
          options_set.url = url
        end
      end

      begin
        options_available.parse!(args)
      rescue Exception => e
        puts "Exception encountered: #{e}"
        puts options_available
        exit 1
      end

      options_set
    end
  end
end

class Scrapper
  def initialize(url)
    @page = open(url).read
  end

  def title
    $1 if @page =~ /<title>(.+)<\/title>/
  end
end

def run!
  page = Scrapper.new Options.parse(ARGV).url
  (page.title) ? puts(page.title) : 'A title for the document was not found.'
end

run! if __FILE__ == $0
