
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/parse_url'

describe Options do
  describe 'parses allowed options' do
    it 'builds a struct with params --url URL' do
      params = ['--url', 'http://some.url']
      result = Options.parse([params[0], params[1]])

      result.url.must_equal params[1]
    end

    it 'builds a struct with params -u URL' do
      params = ['-u', 'http://some.url']
      result = Options.parse([params[0], params[1]])

      result.url.must_equal params[1]
    end
  end

  describe 'does not parse unknown options' do
    it 'builds an empty struct with no params option' do
      params   = ['http://some.url']
      expected = "#<OpenStruct>"

      Options.parse([params[0]]).inspect.must_equal expected
    end

    it 'displays usage when an unknown option is received' do
      skip 'comment «exit 1» inside the rescue block before testing'
      params   = ['--uri']
      expected = "Exception encountered: invalid option: --uri\n" \
                 "Usage: url\n" \
                 "    -u, --url URL                    Fetches the given url\n"

      lambda { Options.parse([params[0]]) }.must_output expected
    end
  end
end

describe Scrapper do
  describe '#title' do
    before do
      # Stubbing the network calls
      class String
        def read() self; end
      end
    end

    it 'gets the content of a document that has a <title> element' do
      # Stubbing the network calls
      class Scrapper
        def open(url) '<title>Hello, world!</title>'; end
      end

      document = Scrapper.new 'Some url'
      expected = 'Hello, world!'
      document.title.must_equal expected
    end

    it 'return nil when a document has no <title> element' do
      # Stubbing the network calls
      class Scrapper
        def open(url) '<h1>I have no title</h1'; end
      end

      document = Scrapper.new 'Some url'
      expected = nil
      document.title.must_equal expected
    end
  end
end
