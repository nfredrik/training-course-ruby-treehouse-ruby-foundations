
# Example on the use of Observable.

require 'observer'

class BankAccount
  include Observable

  def initialize(name)
    @name    = name
    @balance = 0
  end

  def deposit(amount)
    @balance += amount
    changed
    notify_observers(Time.now, 'deposit', amount)
  end

  def withdraw(amount)
    @balance -= amount
    changed
    notify_observers(Time.now, 'withdrawal', amount)
  end
end

class AccountOberver
  def initialize(account)
    @account = account
    @account.add_observer(self)
  end

  def update(time, kind, amount)
    puts "[#{time} (#{kind})]: #{amount}"
  end
end

account = BankAccount.new('David')
AccountOberver.new(account)

account.deposit  100
account.withdraw  50
