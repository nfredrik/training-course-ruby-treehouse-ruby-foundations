# Treehouse - Ruby Foundations
Exercises and examples from the 'Ruby Foundations' course, imparted by Jason
Seifer at [Treehouse](http://teamtreehouse.com/).

## Topics
- **Ruby Basics**
  Up and running with Ruby and to have you writing your first Ruby programs in
  no time!
- **Objects, Classes and Variables**
  You'll learn about how to tell the Ruby interpreter what to do, set variables,
  and create and instantiate classes.
- **Strings**
  How to create and work with strings, learn about special characters and
  escape, and learn about common and useful methods available when working with
  strings.
- **Numbers**
  About integers, floating point numbers, math, and more. You'll learn how to
  create and work with numbers, compare numbers, and work with currency.
- **Arrays**
  What arrays are and get you comfortable with creating and working with arrays.
- **Hashes**
  Creating, querying, and working with hashes.
- **Methods**
  A method is a set of expressions that return a value. Learning how to create
  your own methods is the basis for almost all programming you'll do in Ruby.
- **Loops**
  A loop is a series of statements that can be repeated several times. The Ruby
  language has several different kinds of loops available when writing programs.
- **Blocks**
  Blocks are a special kind of syntax in Ruby. A block is a way of grouping
  statements together that is executed when Ruby encounters the method. 
- **Procs & Lambdas**
  Procs & Lambdas are blocks of code that are assigned to variables. In this
  way, procs and lambdas can be passed around to different methods and the scope
  can change.
- **Modules**
  Modules serve as containers for data, classes, methods, or even other modules.
  Modules are very useful for sharing behavior between your classes and are
  frequently used for namespacing classes and constants in your programs.
- **Ruby Core**
  The core collection of classes and modules helps out when coding Ruby
  programs.
- **Ruby Standard Library**
  The standard library contains modules and classes that are commonly used for
  writing programs. This includes functionality for testing, configuration,
  option parsing, benchmarking, and more.
- **Testing**
  Automated testing is the process of writing code that tests different behavior
  of your programs. The Ruby standard library includes a library called MiniTest
  which can be used to write automated tests.

---
This repository contains portions of code from the TreehouseTeam courses and is included only for reference under _fair use_.