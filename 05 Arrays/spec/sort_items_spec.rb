
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/sort_items'

describe '#sorted_print' do
  it 'prints arrays items in a sorted order' do
    arr = 10.times.map { rand 10 + 1 }
    out = arr.sort.to_s
    lambda { sorted_print(arr) }.must_output out
  end
end
