
# Input and Printing
# Write a small Ruby program that asks you what your name is. Have the program
# print out the number of characters in your name. Also have the program print
# out a message if your name is longer than 25 characters.

def run!
  print 'Hello, what is you name? '
  eval_name(gets.chomp)
end

def eval_name(name)
  puts "Your name has #{name.size} letters."
  puts "You have a long name!" if name.size > 25
end

# Executes the code if the program is called from this file.
run! if __FILE__ == $0
