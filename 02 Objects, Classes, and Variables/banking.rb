
# Example that shows the use of objects and classes.

# class definition
class BankAccount
  def initialize(name)
    @transactions = []
    @balance      = 0
  end

  def deposit
    print 'How much would you like to deposit? '
    amount    = gets.chomp
    @balance += amount.to_f
    puts  "$#{amount} deposited."
  end

  def show_balance
    puts "Your balance is #{@balance}"
  end
end

# instance of the class
bank_account = BankAccount.new('David Rojo')
bank_account.class # => BankAccount

# instance methods
bank_account.deposit
bank_account.show_balance
