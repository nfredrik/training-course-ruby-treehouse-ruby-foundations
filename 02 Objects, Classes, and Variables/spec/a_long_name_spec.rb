
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/a_long_name'

describe '#eval_name' do
  it 'shows the name size when the size is up to 25 chars' do
    name   = 'David Rojo'
    output = "Your name has #{name.size} letters.\n"
    lambda { eval_name(name) }.must_output output
  end

  it 'also shows an extra message if the name has more than 25 chars' do
    name   = 'David Olimpo Rojo Villanueva'
    output = "Your name has #{name.size} letters.\n" \
             "You have a long name!\n"
    lambda { eval_name(name) }.must_output output
  end
end
