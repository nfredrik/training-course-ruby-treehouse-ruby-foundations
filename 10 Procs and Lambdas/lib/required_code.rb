
# Try writing a class that takes a method which requires a proc or lambda as
# input. Save the proc or lambda internally and then call it from another
# method.

class Reusable
  def store(code = nil, &block)
    @code = block_given? ? block : code
  end

  def reuse
    @code
  end
end

def run!
  hello   = lambda { |name| "Hello, #{name}!" }
  goodbye = proc   { |name| (name) ? "Goodbye, #{name}!" : "Goodbye." }

  chest = Reusable.new

  chest.store hello
  puts chest.reuse.call 'David'

  chest.store goodbye
  puts chest.reuse.call 'David'
  puts chest.reuse.call

  chest.store do |n|
    n.times { |i| print "#{i + 1} " }
    puts
  end
  chest.reuse.call 10
end

run! if __FILE__ == $0
