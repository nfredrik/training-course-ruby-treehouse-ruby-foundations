
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/required_code'

describe Reusable do
  subject { Reusable.new }

  it 'has a #store method' do
    subject.must_respond_to 'store'
  end

  it 'has a #reuse method' do
    subject.must_respond_to 'reuse'
  end

  specify 'stores a proc/lambda for later use' do
    test = lambda { 'x' }
    subject.store test
    subject.reuse.call.must_equal test.call
  end

  specify 'stores a block for later use' do
    test = lambda { 10 }
    subject.store { 10 }
    subject.reuse.call.must_equal test.call
  end
end
